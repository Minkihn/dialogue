#ifndef CHARACTER_HXX
#define CHARACTER_HXX
#include <string>

class Character
{
    public:
        Character(unsigned int id, std::string name);
        const unsigned int& getId();
        const std::string& getName();


    private:
        unsigned int    mId;
        std::string     mName;
};

#endif // CHARACTER_HXX
