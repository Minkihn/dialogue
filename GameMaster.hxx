#ifndef GAMEMASTER_HXX
#define GAMEMASTER_HXX
#include <list>
#include "DialogTree.hxx"
#include "ConsoleDialog.hxx"
#include "Character.hxx"

class GameMaster
{
    public:
        GameMaster();
        ~GameMaster();
        void begin();

    private:
        DialogTree              mDt;
        ConsoleDialog*          mCd;
        std::list<Character*>   mCharacters;
};

#endif // GAMEMASTER_HXX
