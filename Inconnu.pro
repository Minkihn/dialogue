TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    main.cxx \
    Node.cxx \
    DialogTree.cxx \
    ConsoleDialog.cxx \
    Character.cxx \
    GameMaster.cxx

HEADERS += \
    Node.hxx \
    DialogTree.hxx \
    ConsoleDialog.hxx \
    Character.hxx \
    GameMaster.hxx

