#include "ConsoleDialog.hxx"
#include <iostream>
#include <vector>

ConsoleDialog::ConsoleDialog(DialogTree& dt) : mDt(dt)
{
    mCurrentNode = dt.get(0);
    mRunning = true;
}

void ConsoleDialog::begin()
{
    if (mCurrentNode->getText() == "END")
        mRunning = false;

    while (mRunning)
    {
        std::cout << "  " << mCurrentNode->getOwnerId() << ": " << mCurrentNode->getText() << std::endl;

        std::vector<int> targets = mDt.getTargets( mCurrentNode->getId() );

        if (targets.size() == 1)
        {
            if (targets[0] == 0)
                mRunning = false;

            mCurrentNode = mDt.get( targets[0] );
        }
        else if (targets.size() > 1)
        {
            unsigned int i, choice;
            bool invalid_input = true;

            for (i = 0; i < targets.size(); ++i)
                std::cout << i + 1 << ". " << mDt.get( targets[i] - 1 )->getText() << std::endl;

            while (invalid_input)
            {
                std::cin >> choice;

                if (choice >= 1 && choice <= i)
                    invalid_input = false;
            }

            if (mDt.getTargets(targets[choice - 1])[0] == 0)
                break;

            mCurrentNode = mDt.get( mDt.getTargets(targets[choice - 1])[0] - 1 );

        }
        else
        {
            std::cerr << "Chaining error!" << std::endl;
            mRunning = false;
        }
    }

    std::cout << "Dialog has ended!" << std::endl;
}
