#ifndef DIALOG_HXX
#define DIALOG_HXX
#include "DialogTree.hxx"

class ConsoleDialog
{
    public:
        ConsoleDialog(DialogTree& dt);
        void begin();

    private:
        DialogTree& mDt;
        Node*       mCurrentNode;
        bool        mRunning;
};

#endif // DIALOG_HXX
