#ifndef NODE_HXX
#define NODE_HXX
#include <string>
#include <list>

class Node
{
    public:
        Node(int id, int owner_id, std::string text);
        const int& getId()             {return mId;}
        const int& getOwnerId()        {return mOwnerId;}
        const std::string& getText()   {return mText;}

    private:
        int         mId;
        int         mOwnerId;
        std::string mText;

        std::list<Node*> mTargets;
};

#endif // NODE_HXX
