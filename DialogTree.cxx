#include "DialogTree.hxx"
#include <iostream>
#include "Node.hxx"



DialogTree::DialogTree()
{
    Node* first     = new Node(1, 2, "Hello.");
    Node* second    = new Node(2, 1, "Goodbye.");
    Node* third     = new Node(3, 1, "Farewell. By the way, what is your name?");
    Node* fourth    = new Node(4, 2, "Kalanna.");

    *this << first << second << third << fourth;
    this->join(1, 2);
    this->join(1, 3);
    this->join(3, 4);
    this->join(2, "END");
    this->join(4, "END");
}

DialogTree::~DialogTree()
{
    std::vector<Node*>::iterator it = mNodes.begin();

    for (; it != mNodes.end(); )
    {
        delete *it;
        it = mNodes.erase(it);
    }
}

Node* DialogTree::get(int id)
{
    return mNodes[id];
}

std::vector<int> DialogTree::getTargets(const int& id)
{
    return mJoints[id];
}

DialogTree& DialogTree::operator<<(Node* node)
{
    std::cout << "Adding node " << node->getId() << ": " << node->getText() << std::endl;

    mNodes.push_back(node);
    return *this;
}

void DialogTree::join(int src, int dst)
{
    std::cout << "Adding joint " << src << ", " << dst << std::endl;

    mJoints[src].push_back(dst);
}

void DialogTree::join(int src, std::string spc_dst)
{
    if (spc_dst == "END")
    {
        std::cout << "Adding joint " << src << ", END" << std::endl;
        mJoints[src].push_back(0);
    }
}

