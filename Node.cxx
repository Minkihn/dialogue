#include "Node.hxx"
#include <string>

Node::Node(int id, int owner_id, std::string text) : mText(text)
{
    mId      = id;
    mOwnerId = owner_id;
}
