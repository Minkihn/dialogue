#include "GameMaster.hxx"
#include "ConsoleDialog.hxx"

GameMaster::GameMaster()
{
    mCd = new ConsoleDialog(mDt);
    Character* player = new Character(1, "Player");
    Character* npc    = new Character(2, "Kalanna");

    mCharacters.push_back(player);
    mCharacters.push_back(npc);
}

GameMaster::~GameMaster()
{
    std::list<Character*>::iterator it = mCharacters.begin();

    for (; it != mCharacters.end(); )
    {
        delete *it;
        it = mCharacters.erase(it);
    }
    delete mCd;
}

void GameMaster::begin()
{
    mCd->begin();
}
