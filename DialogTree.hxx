#ifndef DIALOGTREE_HXX
#define DIALOGTREE_HXX
#include "Node.hxx"
#include <vector>
#include <map>

class DialogTree
{
    public:
        DialogTree();
        ~DialogTree();
        Node* get(int id);
        std::vector<int> getTargets(const int& id);
        DialogTree& operator<<(Node* node);
        void join(int src, int dst);
        void join(int src, std::string spc_dst);

    private:
        std::vector<Node*> mNodes;
        std::map<int, std::vector<int> > mJoints;
};

#endif // DIALOGTREE_HXX
