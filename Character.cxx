#include "Character.hxx"
#include <string>

Character::Character(unsigned int id, std::string name) : mName(name)
{
    mId = id;
}

const unsigned int& Character::getId()
{
    return mId;
}

const std::string& Character::getName()
{
    return mName;
}
